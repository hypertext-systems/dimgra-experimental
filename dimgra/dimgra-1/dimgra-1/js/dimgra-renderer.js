/* Copyright (C) 2019-2020 Stephan Kreutzer
 *
 * This file is part of dimgra-1.
 *
 * dimgra-1 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * dimgra-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with dimgra-1. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function UpdateDimensions(dimensions, target, currentSelection)
{
    let axisControl = document.getElementById(target);

    for (let i = axisControl.children.length - 1; i >= 0; i--)
    {
        let found = false;

        for (let j = 0; j < dimensions.length; j++)
        {
            let dimension = dimensions[j];

            if (dimension === null)
            {
                dimension = "";
            }

            if (axisControl.children[i].getAttribute("value") === dimension)
            {
                found = true;
                break;
            }
        }

        if (found == false)
        {
            axisControl.removeChild(axisControl.children[i]);
        }
    }

    for (let i = 0; i < dimensions.length; i++)
    {
        let dimension = dimensions[i];

        if (dimension === null)
        {
            dimension = "";
        }

        let found = false;

        for (let j = 0; j < axisControl.children.length; j++)
        {
            if (dimension === axisControl.children[j].getAttribute("value"))
            {
                found = true;
                break;
            }
        }

        if (found == false)
        {
            let caption = document.createTextNode(dimension);

            let option = document.createElement("option");
            option.setAttribute("value", dimension);
            option.appendChild(caption);

            axisControl.appendChild(option);
        }
    }

    return true;
}

function RenderYAxis(before, node, after)
{
    return RenderNodes(new Array(before, node, after), "pane-current", "Current");
}

function RenderXChildren(nodes)
{
    // TODO: Properly check if nodes is an array.
    return RenderNodes(nodes, "pane-right", "Pane-Right");
}

function RenderNodes(nodes, targetId, caption)
{
    // TODO: Properly check if nodes is an array.

    let target = document.getElementById(targetId);

    if (target == null)
    {
        throw "Renderer: Can't find target with ID '" + targetId + "'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode(caption));
    container.appendChild(header);


    if (targetId === "pane-current")
    {
        if (nodes.length >= 3)
        {
            if (nodes.length > 3)
            {
                
            }

            if (nodes[0] !== undefined &&
                nodes[0] !== null)
            {
                let title = document.createTextNode(nodes[0].getTitle());

                let span = document.createElement("span");
                span.setAttribute("onclick", "ShowDetails(\"" + nodes[0].getId() + "\");");
                span.appendChild(title);

                container.appendChild(span);

                container.appendChild(document.createElement("br"));

                let navigationLink = document.createElement("a");
                navigationLink.setAttribute("href", "javascript:void(0);");
                // TODO: Prevent injection (and maybe at other places, too).
                navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[0].getId() + "\", false);");

                let arrow = document.createTextNode("⬇");
                navigationLink.appendChild(arrow);
                container.appendChild(navigationLink);

                container.appendChild(document.createElement("hr"));
            }

            let title = document.createTextNode(nodes[1].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + nodes[1].getId() + "\");");
            span.setAttribute("class", "highlight");
            span.appendChild(title);

            container.appendChild(span);

            if (nodes[2] !== null &&
                nodes[2] !== null)
            {
                container.appendChild(document.createElement("hr"));

                let navigationLink = document.createElement("a");
                navigationLink.setAttribute("href", "javascript:void(0);");
                // TODO: Prevent injection (and maybe at other places, too).
                navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[2].getId() + "\", false);");

                let arrow = document.createTextNode("⬇");
                navigationLink.appendChild(arrow);
                container.appendChild(navigationLink);

                container.appendChild(document.createElement("br"));

                let title = document.createTextNode(nodes[2].getTitle());

                let span = document.createElement("span");
                span.setAttribute("onclick", "ShowDetails(\"" + nodes[2].getId() + "\");");
                span.appendChild(title);

                container.appendChild(span);
            }
        }
        else
        {
            
        }
    }
    else if (targetId === "pane-right")
    {
        for (let i = 0; i < nodes.length; i++)
        {
            let navigationLink = document.createElement("a");
            navigationLink.setAttribute("href", "javascript:void(0);");
            // TODO: Prevent injection (and maybe at other places, too).
            navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[i].getId() + "\", true);");

            // http://xahlee.info/comp/unicode_BLACK_RIGHTWARDS_problem.html
            // let arrow = document.createTextNode("➡");
            let arrow = document.createTextNode("⮕");
            navigationLink.appendChild(arrow);
            container.appendChild(navigationLink);

            container.appendChild(document.createTextNode(" "));

            let title = document.createTextNode(nodes[i].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + nodes[i].getId() + "\");");
            span.appendChild(title);

            container.appendChild(span);

            /* if (i < nodes.length - 1) */
            {
                container.appendChild(document.createElement("hr"));
            }
        }
    }
    else
    {
        throw "Renderer: Unknown target ID '" + targetId + "'.";
    }


    target.appendChild(container);

    return true;
}

function Reset()
{
    let targets = new Array("pane-current", "pane-right")

    for (let i = 0; i < targets.length; i++)
    {
        let target = document.getElementById(targets[i]);

        if (target == null)
        {
            throw "Renderer: Can't find target with ID '" + targets[i] + "'.";
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }
}

function RenderDetails(node)
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Details"));
    container.appendChild(header);

    let description = node.getText();

    if (description != null)
    {
        container.appendChild(document.createTextNode(description));
    }

    target.appendChild(container);

    return 0;
}

function ResetDetails()
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }
}
