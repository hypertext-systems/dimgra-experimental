<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2020 Stephan Kreutzer

This file is part of dimgra-1.

dimgra-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

dimgra-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with dimgra-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:graphml="http://graphml.graphdrawing.org/xmlns">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by graphml_to_dimgra_xhtml_1.xsl of dimgra-1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/hypertext-systems/dimgra-experimental/tree/master/dimgra/dimgra-1/ and https://hypertext-systems.org). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2020 Stephan Kreutzer

This file is part of dimgra-1.

dimgra-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

dimgra-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with dimgra-1. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="graphml-input"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>dimgra-1</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <link rel="stylesheet" type="text/css" href="./css/styles.css"/>

        <!-- <script src="...">//</script> to prevent self-closing by XSLT outputting XHTML. -->
        <script type="text/javascript" src="./js/dimgra-engine.js">//</script>
        <script type="text/javascript" src="./js/dimgra-renderer.js">//</script>
        <script type="text/javascript" src="./js/dimgra-navigation.js">//</script>

        <script type="text/javascript">
        <!-- TODO: Support to reference node based on ID in window.location.hash -->
<xsl:text>
"use strict";

// As browsers/W3C/WHATWG are evil, they might ignore the self-declarative XML
// namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

function loadGraph()
{
    // NavigateNode() with empty ID string: load the "default" (first) node
    // of the first dimension.
    NavigateNode("", false);
}

window.onload = function () {
    let loadLink = document.getElementById('loadLink');
    loadLink.parentNode.removeChild(loadLink);

    loadGraph();

    if ("onhashchange" in window)
    {
        window.onhashchange = loadGraph;
    }
};
</xsl:text>
        </script>
      </head>
      <body>
        <div id="graph">
          <div id="container-current">
            <form>
              <select id="axis-y-dimension" size="1" onchange="ChangeAxisDimension('y');">
              </select>
            </form>
            <div id="pane-current"/>
          </div>
          <div id="container-right">
            <form>
              <select id="axis-x-dimension" size="1" onchange="ChangeAxisDimension('x');">
              </select>
            </form>
            <div id="pane-right"/>
          </div>
          <div id="details"/>
          <div id="loadLink">
            <a href="#" onclick="loadGraph();">Load</a>
          </div>
        </div>

        <!--
            As the stupid browser execution environment is sandboxed, local files can't
            be dynamically read from disk, and as we're forced to load the entire source
            into memory anyway because of this, if it's XML, we could easily put it into
            the DOM instead of parsing XML from a JavaScript variable. This interferes
            with XHTML GUI work in this XSLT file, but if the XML source would need to be
            escaped to be a JavaScript string literal, a conversion is unavoidable, so
            the XHTML file can just as well be the result of a XSLT conversion from the source.
        -->
        <div id="graphml-input" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program. It's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="/graphml:graphml"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//graphml:*">
    <xsl:element name="graphml:{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//graphml:*//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
