<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2023 Stephan Kreutzer

This file is part of dimgra-2.

dimgra-2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

dimgra-2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with dimgra-2. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:graphml="http://graphml.graphdrawing.org/xmlns">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by graphml_to_dimgra_xhtml_1.xsl of dimgra-2, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/hypertext-systems/dimgra-experimental/tree/master/dimgra/dimgra-2/ and https://hypertext-systems.org). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2023 Stephan Kreutzer

This file is part of dimgra-2.

dimgra-2 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

dimgra-2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with dimgra-2. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="graphml-input"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>dimgra-2</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <style type="text/css">
body
{
    font-family: sans-serif;
}

#container-current
{
    width: 48%;
    height: 100%;
    position: absolute;
    top: 0%;
    overflow: auto;
}

#pane-current
{
    text-align: center;
}

#container-right
{
    width: 48%;
    height: 100%;
    position: absolute;
    left: 52%;
    top: 0%;
    overflow: auto;
}

#pane-right
{
    text-align: left;
}

#details
{
    width: 96%;
    height: 38%;
    position: absolute;
    left: 2%;
    top: 62%;
}

#axis-x-dimension
{
    width: 100%;
}

#axis-y-dimension
{
    width: 100%;
}

.highlight
{
    background-color: LightGray;
}
        </style>

        <xsl:comment> Engine </xsl:comment>
        <script type="text/javascript">
"use strict";

function GraphEngine()
{
    let that = this;

    // Pass the ID of the node, which is a string. Empty string to get the first,
    // default node. Returns null if the node can't be obtained.
    // TODO: When requesting the default, the actual ID of the first node should
    // be in the returned Node object.
    that.getNodeById = function(id) {
        if (typeof id !== 'string' &amp;&amp; !(id instanceof String))
        {
            id = "";
        }

        if (_loaded == false)
        {
            _load();
        }

        if (_loaded == false)
        {
            throw "Loading failed."
        }

        let nodeSource = null;

        if ("querySelectorAll" in document)
        {
            if (id.length &lt;= 0)
            {
                // Select first.

                // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
                // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
                let result = document.querySelectorAll("node");

                for (let i = 0; i &lt; result.length; i++)
                {
                    if (result[i].namespaceURI == "http://graphml.graphdrawing.org/xmlns")
                    {
                        nodeSource = result[i];
                        break;
                    }
                }
            }
            else
            {
                // Select as requested by id paramenter.

                // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
                // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
                let result = document.querySelectorAll("node[id='" + id + "']");
                let nodes = new Array();

                for (let i = 0; i &lt; result.length; i++)
                {
                    if (result[i].namespaceURI == "http://graphml.graphdrawing.org/xmlns")
                    {
                        nodes.push(result[i]);
                    }
                }

                if (nodes.length &lt;= 0)
                {

                }
                else if (nodes.length &gt; 1)
                {
                    throw "'id' attribute value '" + id + "' of a node found more than once.";
                }

                nodeSource = nodes[0];
            }
        }
        else
        {
            if (id.length &lt;= 0)
            {
                if (_nodeMapping.length &gt; 0)
                {
                    for (let item in _nodeMapping)
                    {
                        nodeSource = _nodeMapping[item];
                        break;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (!(id in _nodeMapping))
                {
                    return null;
                }

                nodeSource = _nodeMapping[id];
            }
        }

        if (nodeSource == null)
        {
            return null;
        }

        if (nodeSource.hasAttribute("id") != true)
        {
            throw "node is missing its 'id' attribute.";
        }

        let title = null;
        let description = null;

        for (let i = 0; i &lt; nodeSource.children.length; i++)
        {
            let child = nodeSource.children[i];

            if (child.namespaceURI != "http://graphml.graphdrawing.org/xmlns")
            {
                continue;
            }

            if (child.localName != "data")
            {
                continue;
            }

            if (child.hasAttribute("key") != true)
            {
                throw "data is missing its 'key' attribute.";
            }

            if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/node-title.20200601T122500Z")
            {
                if (title != null)
                {
                    throw "node contains more than one title.";
                }

                title = child.textContent;
            }
            else if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/node-text.20200601T122500Z")
            {
                if (description != null)
                {
                    throw "node contains more than one text.";
                }

                description = child.textContent;
            }
        }


        let parents = new Array();
        let children = new Array();

        if ("querySelectorAll" in document)
        {
            // TODO: Check nodeSource.getAttribute("id") for injection here and elsewhere.
            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            let result = document.querySelectorAll("edge[source='" + nodeSource.getAttribute("id") + "']");

            for (let i = 0; i &lt; result.length; i++)
            {
                if (result[i].namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                {
                    continue;
                }

                if (result[i].hasAttribute("source") != true)
                {
                    throw "edge is missing its 'source' attribute.";
                }

                if (result[i].hasAttribute("target") != true)
                {
                    throw "edge is missing its 'target' attribute.";
                }

                let dimension = null;
                let dimensionEstablishing = null;

                for (let j = 0; j &lt; result[i].children.length; j++)
                {
                    let child = result[i].children[j];

                    if (child.namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                    {
                        continue;
                    }

                    if (child.localName != "data")
                    {
                        continue;
                    }

                    if (child.hasAttribute("key") != true)
                    {
                        throw "data is missing its 'key' attribute.";
                    }

                    if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-target.20200601T122500Z")
                    {
                        if (dimension != null)
                        {
                            throw "edge contains more than one dimension.";
                        }

                        dimension = child.textContent;
                    }
                    else if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-establishing.20200601T122500Z")
                    {
                        if (dimensionEstablishing != null)
                        {
                            throw "edge contains more than one setting about if it is establishing a dimension.";
                        }

                        if (child.textContent == "true")
                        {
                            dimensionEstablishing = true;
                        }
                        else if (child.textContent == "false")
                        {
                            dimensionEstablishing = false;
                        }
                        else
                        {
                            throw "edge contains unknown setting value '" + child.textContent + "' regarding if it's establishing a dimension, 'true' or 'false' (bool) expected.";
                        }
                    }

                    if (dimension != null &amp;&amp;
                        dimensionEstablishing != null)
                    {
                        break;
                    }
                }

                if (dimension === null)
                {
                    dimension = "";
                    dimensionEstablishing = false;
                }

                if (dimensionEstablishing === null)
                {
                    dimensionEstablishing = false;
                }

                children.push(new Edge(result[i].getAttribute("target"), dimension, dimensionEstablishing));
            }


            // TODO: Check nodeSource.getAttribute("id") for injection here and elsewhere.
            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            result = document.querySelectorAll("edge[target='" + nodeSource.getAttribute("id") + "']");

            for (let i = 0; i &lt; result.length; i++)
            {
                if (result[i].namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                {
                    continue;
                }

                if (result[i].hasAttribute("source") != true)
                {
                    throw "edge is missing its 'source' attribute.";
                }

                if (result[i].hasAttribute("target") != true)
                {
                    throw "edge is missing its 'target' attribute.";
                }

                let dimension = null;
                let dimensionEstablishing = null;

                for (let j = 0; j &lt; result[i].children.length; j++)
                {
                    let child = result[i].children[j];

                    if (child.namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                    {
                        continue;
                    }

                    if (child.localName != "data")
                    {
                        continue;
                    }

                    if (child.hasAttribute("key") != true)
                    {
                        throw "data is missing its 'key' attribute.";
                    }

                    if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-target.20200601T122500Z")
                    {
                        if (dimension != null)
                        {
                            throw "edge contains more than one dimension.";
                        }

                        dimension = child.textContent;
                    }
                    else if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-establishing.20200601T122500Z")
                    {
                        if (dimensionEstablishing != null)
                        {
                            throw "edge contains more than one setting about if it is establishing a dimension.";
                        }

                        if (child.textContent == "true")
                        {
                            dimensionEstablishing = true;
                        }
                        else if (child.textContent == "false")
                        {
                            dimensionEstablishing = false;
                        }
                        else
                        {
                            throw "edge contains unknown setting value '" + child.textContent + "' regarding if it's establishing a dimension, 'true' or 'false' (bool) expected.";
                        }
                    }

                    if (dimension != null &amp;&amp;
                        dimensionEstablishing != null)
                    {
                        break;
                    }
                }

                /*
                if (dimensionEstablishing !== true)
                {
                    dimension = null;
                }
                */

                if (dimension === null)
                {
                    dimension = "";
                    dimensionEstablishing = false;
                }

                if (dimensionEstablishing === null)
                {
                    dimensionEstablishing = false;
                }

                parents.push(new Edge(result[i].getAttribute("source"), dimension, dimensionEstablishing));
            }
        }
        else
        {
            // TODO: Get nodes from the native DOM XML loader.
        }

        return new Node(nodeSource.getAttribute("id"), title, description, parents, children);
    }

    function _load()
    {
        if (_loaded != false)
        {
            return true;
        }

        if ("querySelectorAll" in document)
        {
            _loaded = true;
            return true;
        }
        else
        {
            //TODO Implement native DOM XML loader.
        }
    }

    let _loaded = false;
}

function Node(id, title, description, parents, children)
{
    // TODO: Properly check if parents, children is an Array,
    // and throw exception if not.

    let that = this;

    that.getId = function()
    {
        return _id;
    }

    that.getTitle = function()
    {
        return _title;
    }

    that.getText = function()
    {
        return _description;
    }

    that.getParents = function()
    {
        return _parents;
    }

    that.getChildren = function()
    {
        return _children;
    }

    let _id = id;
    let _title = title;
    let _description = description;
    // Edge objects.
    let _parents = parents;
    // Edge objects.
    let _children = children;
}

function Edge(id, dimension, dimensionEstablishing)
{
    let that = this;

    that.getId = function()
    {
        return _id;
    }

    that.getDimension = function()
    {
        return _dimension;
    }

    that.isDimensionEstablishing = function()
    {
        return _dimensionEstablishing;
    }

    let _id = id;
    let _dimension = dimension;
    let _dimensionEstablishing = dimensionEstablishing;
}
        </script>

        <xsl:comment> Renderer </xsl:comment>
        <script type="text/javascript">
"use strict";

function UpdateDimensions(dimensions, target, currentSelection)
{
    let axisControl = document.getElementById(target);

    for (let i = axisControl.children.length - 1; i &gt;= 0; i--)
    {
        let found = false;

        for (let j = 0; j &lt; dimensions.length; j++)
        {
            let dimension = dimensions[j];

            if (dimension === null)
            {
                dimension = "";
            }

            if (axisControl.children[i].getAttribute("value") === dimension)
            {
                found = true;
                break;
            }
        }

        if (found == false)
        {
            axisControl.removeChild(axisControl.children[i]);
        }
    }

    for (let i = 0; i &lt; dimensions.length; i++)
    {
        let dimension = dimensions[i];

        if (dimension === null)
        {
            dimension = "";
        }

        let found = false;

        for (let j = 0; j &lt; axisControl.children.length; j++)
        {
            if (dimension === axisControl.children[j].getAttribute("value"))
            {
                found = true;
                break;
            }
        }

        if (found == false)
        {
            let caption = document.createTextNode(dimension);

            let option = document.createElement("option");
            option.setAttribute("value", dimension);
            option.appendChild(caption);

            axisControl.appendChild(option);
        }
    }

    return true;
}

function RenderYAxis(before, node, after)
{
    return RenderNodes(new Array(before, node, after), "pane-current", "Current");
}

function RenderXChildren(nodes)
{
    // TODO: Properly check if nodes is an array.
    return RenderNodes(nodes, "pane-right", "Pane-Right");
}

function RenderNodes(nodes, targetId, caption)
{
    // TODO: Properly check if nodes is an array.

    let target = document.getElementById(targetId);

    if (target == null)
    {
        throw "Renderer: Can't find target with ID '" + targetId + "'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode(caption));
    container.appendChild(header);


    if (targetId === "pane-current")
    {
        if (nodes.length &gt;= 3)
        {
            if (nodes.length &gt; 3)
            {

            }

            if (nodes[0] !== undefined &amp;&amp;
                nodes[0] !== null)
            {
                let title = document.createTextNode(nodes[0].getTitle());

                let span = document.createElement("span");
                span.setAttribute("onclick", "ShowDetails(\"" + nodes[0].getId() + "\");");
                span.appendChild(title);

                container.appendChild(span);

                container.appendChild(document.createElement("br"));

                let navigationLink = document.createElement("a");
                navigationLink.setAttribute("href", "javascript:void(0);");
                // TODO: Prevent injection (and maybe at other places, too).
                navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[0].getId() + "\", false, false);");

                let arrow = document.createTextNode("⬇");
                navigationLink.appendChild(arrow);
                container.appendChild(navigationLink);

                container.appendChild(document.createElement("hr"));
            }

            let title = document.createTextNode(nodes[1].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + nodes[1].getId() + "\");");
            span.setAttribute("class", "highlight");
            span.appendChild(title);

            container.appendChild(span);

            if (nodes[2] !== null &amp;&amp;
                nodes[2] !== null)
            {
                container.appendChild(document.createElement("hr"));

                let navigationLink = document.createElement("a");
                navigationLink.setAttribute("href", "javascript:void(0);");
                // TODO: Prevent injection (and maybe at other places, too).
                navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[2].getId() + "\", false, false);");

                let arrow = document.createTextNode("⬇");
                navigationLink.appendChild(arrow);
                container.appendChild(navigationLink);

                container.appendChild(document.createElement("br"));

                let title = document.createTextNode(nodes[2].getTitle());

                let span = document.createElement("span");
                span.setAttribute("onclick", "ShowDetails(\"" + nodes[2].getId() + "\");");
                span.appendChild(title);

                container.appendChild(span);
            }
        }
        else
        {

        }
    }
    else if (targetId === "pane-right")
    {
        for (let i = 0; i &lt; nodes.length; i++)
        {
            let navigationLink = document.createElement("a");
            navigationLink.setAttribute("href", "javascript:void(0);");
            // TODO: Prevent injection (and maybe at other places, too).
            navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[i].getId() + "\", true, false);");

            // http://xahlee.info/comp/unicode_BLACK_RIGHTWARDS_problem.html
            // let arrow = document.createTextNode("➡");
            let arrow = document.createTextNode("⮕");
            navigationLink.appendChild(arrow);
            container.appendChild(navigationLink);

            container.appendChild(document.createTextNode(" "));

            let title = document.createTextNode(nodes[i].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + nodes[i].getId() + "\");");
            span.appendChild(title);

            container.appendChild(span);

            /* if (i &lt; nodes.length - 1) */
            {
                container.appendChild(document.createElement("hr"));
            }
        }
    }
    else
    {
        throw "Renderer: Unknown target ID '" + targetId + "'.";
    }


    target.appendChild(container);

    return true;
}

function Reset()
{
    let targets = new Array("pane-current", "pane-right")

    for (let i = 0; i &lt; targets.length; i++)
    {
        let target = document.getElementById(targets[i]);

        if (target == null)
        {
            throw "Renderer: Can't find target with ID '" + targets[i] + "'.";
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }
}

function RenderDetails(node)
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Details"));
    container.appendChild(header);

    let description = node.getText();

    if (description != null)
    {
        container.appendChild(document.createTextNode(description));
    }

    target.appendChild(container);
    target.style.display = "block";

    target = document.getElementById("container-right");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-right\".";
    }

    target.style.height = "62%";

    target = document.getElementById("container-current");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-current\".";
    }

    target.style.height = "62%";

    return 0;
}

function ResetDetails()
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID 'details'.";
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }

    target.style.display = "none";

    target = document.getElementById("container-right");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-right\".";
    }

    target.style.height = "100%";

    target = document.getElementById("container-current");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-current\".";
    }

    target.style.height = "100%";
}
        </script>

        <xsl:comment> Navigation </xsl:comment>
        <script type="text/javascript">
"use strict";

let currentNode = null;
let currentXAxisDimension = null;
let currentYAxisDimension = null;
let graphEngine = new GraphEngine();

// TODO: This should be optimized by caching xParents and xChildren
// that were already loaded/interpreted for the current node.

function NavigateNode(nodeId, dimensionalPivot, dimensionsReset)
{
    currentNode = null;

    if (dimensionsReset === true)
    {
        currentXAxisDimension = null;
        currentYAxisDimension = null;
    }

    ResetDetails();
    Reset();

    let node = graphEngine.getNodeById(nodeId);

    if (node == null)
    {
        throw "Loading a node failed.";
    }

    if (dimensionalPivot == true)
    {
        currentYAxisDimension = currentXAxisDimension;
        currentXAxisDimension = null;
    }
    else
    {
        let found = false;

        for (let i = 0; i &lt; node.getChildren().length; i++)
        {
            let dimension = node.getChildren()[i].getDimension();

            if (dimension === null)
            {
                dimension = "";
            }

            if (currentYAxisDimension !== null)
            {
                if (dimension === currentYAxisDimension)
                {
                    continue;
                }
            }

            if (dimension === currentXAxisDimension)
            {
                found = true;
                break;
            }
        }

        if (found === false)
        {
            currentXAxisDimension = null;
        }
    }

    if (currentYAxisDimension === null)
    {
        for (let i = 0; i &lt; node.getParents().length; i++)
        {
            if (node.getParents()[i].isDimensionEstablishing() === true)
            {
                currentYAxisDimension = node.getParents()[i].getDimension();

                if (currentYAxisDimension === null)
                {
                    currentYAxisDimension = "";
                }

                break;
            }
        }

        if (currentYAxisDimension === null)
        {
            for (let i = 0; i &lt; node.getChildren().length; i++)
            {
                if (node.getChildren()[i].isDimensionEstablishing() === true)
                {
                    currentYAxisDimension = node.getChildren()[i].getDimension();

                    if (currentYAxisDimension === null)
                    {
                        currentYAxisDimension = "";
                    }

                    break;
                }
            }
        }

        if (currentYAxisDimension === null)
        {
            // No prior knowledge about dimensions pointing inwards.
            currentYAxisDimension = "";
        }
    }

    if (currentXAxisDimension === null)
    {
        for (let i = 0; i &lt; node.getChildren().length; i++)
        {
            if (currentYAxisDimension !== null)
            {
                if (node.getChildren()[i].getDimension() === currentYAxisDimension)
                {
                    continue;
                }
            }

            currentXAxisDimension = node.getChildren()[i].getDimension();

            if (currentXAxisDimension === null)
            {
                currentXAxisDimension = "";
            }

            break;
        }
    }


    let xChildren = new Array();
    let yChildren = new Array();
    let xDimensions = new Array();

    for (let i = 0; i &lt; node.getChildren().length; i++)
    {
        let dimension = node.getChildren()[i].getDimension();

        if (dimension === null)
        {
            dimension = "";
        }

        if (currentYAxisDimension !== null)
        {
            if (dimension === currentYAxisDimension)
            {
                if (yChildren.length &gt; 1)
                {
                    console.log("y-axis with more than one child.");
                    continue;
                }

                yChildren.push(graphEngine.getNodeById(node.getChildren()[i].getId()));
                continue;
            }
        }

        if (currentXAxisDimension !== null)
        {
            if (dimension === currentXAxisDimension)
            {
                xChildren.push(graphEngine.getNodeById(node.getChildren()[i].getId()));
            }
        }

        if (xDimensions.includes(node.getChildren()[i].getDimension()) != true)
        {
            xDimensions.push(node.getChildren()[i].getDimension());
        }
    }


    {
        let before = null;
        let after = null;

        if (yChildren.length &gt; 0)
        {
            after = yChildren[0];
        }

        for (let i = 0; i &lt; node.getParents().length; i++)
        {
            if (node.getParents()[i].isDimensionEstablishing() === true)
            {
                if (node.getParents()[i].getDimension() === currentYAxisDimension)
                {
                    before = graphEngine.getNodeById(node.getParents()[i].getId());
                    break;
                }
            }
        }

        if (RenderYAxis(before, node, after, currentYAxisDimension) == true)
        {
            currentNode = node.getId();
        }
        else
        {
            return false;
        }
    }



    if (UpdateDimensions(xDimensions, "axis-x-dimension", currentXAxisDimension) != true)
    {
        return false;
    }

    if (RenderXChildren(xChildren, currentXAxisDimension) != true)
    {
        return false;
    }

    if (UpdateDimensions(new Array(currentYAxisDimension), "axis-y-dimension", currentYAxisDimension) != true)
    {
        return false;
    }

    return true;
}

function ShowDetails(nodeId)
{
    ResetDetails();

    let node = graphEngine.getNodeById(nodeId);

    if (node == null)
    {
        throw "Loading a node failed.";
    }

    if (RenderDetails(node) != 0)
    {
        return -1;
    }

    return 0;
}

function ChangeAxisDimension(axis)
{
    if (axis == "x")
    {
        let axisControl = document.getElementById("axis-x-dimension");

        if (axisControl == null)
        {

        }

        currentXAxisDimension = axisControl.options[axisControl.selectedIndex].value
    }
    else if (axis == "y")
    {
        let axisControl = document.getElementById("axis-y-dimension");

        if (axisControl == null)
        {

        }

        currentYAxisDimension = axisControl.options[axisControl.selectedIndex].value
    }
    else
    {
        throw "Unknown axis '" + axis + "'.";
    }

    NavigateNode(currentNode, false, false);
}
        </script>

        <script type="text/javascript">
<!-- TODO: Expand keyboard commands to arrow right key to move the gray box
     selector to the right panel/dimension and arrow left key back to the left
     panel/dimension, as well as supporting shift + arrow down/up key to toggle
     between the dimensions in the right panel dropdown menu. -->
<!-- TODO: Add this too to the other dimgra variants. -->
<xsl:text>
"use strict";

// As browsers/W3C/WHATWG are evil, they might ignore the self-declarative XML
// namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

function loadGraph()
{
    if (window.location.hash.length &gt; 1)
    {
        NavigateNode(window.location.hash.substr(1), false, true);
    }
    else
    {
        // NavigateNode() with empty ID string: load the "default" (first) node
        // of the first dimension.
        NavigateNode("", false, true);
    }
}

window.onload = function () {
    let loadLink = document.getElementById('loadLink');
    loadLink.parentNode.removeChild(loadLink);

    loadGraph();

    if ("onhashchange" in window)
    {
        window.onhashchange = loadGraph;
    }

    document.addEventListener("keydown", (event) => {

        if (event.key == "ArrowUp")
        {
            let previousNode = null;

            if (currentNode == null)
            {
                return 1;
            }
            else
            {
                let node = graphEngine.getNodeById(currentNode);
                let parents = node.getParents();

                for (let i = 0, max = parents.length; i &lt; max; i++)
                {
                    if (parents[i].getDimension() == currentYAxisDimension &amp;&amp;
                        parents[i].isDimensionEstablishing() == true)
                    {
                        previousNode = parents[i];
                        break;
                    }
                }

                if (previousNode != null)
                {
                    NavigateNode(previousNode.getId(), false, false);
                }
            }
        }
        else if (event.key == "ArrowDown")
        {
            let nextNode = null;

            if (currentNode == null)
            {
                // This is probably never true, as the document at start
                // navigates to the first node.

                nextNode = graphEngine.getNodeById("");
            }
            else
            {
                let node = graphEngine.getNodeById(currentNode);
                let children = node.getChildren();

                for (let i = 0, max = children.length; i &lt; max; i++)
                {
                    if (children[i].getDimension() == currentYAxisDimension &amp;&amp;
                        children[i].isDimensionEstablishing() == true)
                    {
                        nextNode = children[i];
                        break;
                    }
                }
            }

            if (nextNode != null)
            {
                NavigateNode(nextNode.getId(), false, false);
            }
        }
    });
};
</xsl:text>
        </script>
      </head>
      <body>
        <div id="graph">
          <div id="container-current">
            <form>
              <select id="axis-y-dimension" size="1" onchange="ChangeAxisDimension('y');">
              </select>
            </form>
            <div id="pane-current"/>
          </div>
          <div id="container-right">
            <form>
              <select id="axis-x-dimension" size="1" onchange="ChangeAxisDimension('x');">
              </select>
            </form>
            <div id="pane-right"/>
          </div>
          <div id="details"/>
          <div id="loadLink">
            <a href="#" onclick="loadGraph();">Load</a>
          </div>
        </div>

        <!--
            As the stupid browser execution environment is sandboxed, local files can't
            be dynamically read from disk, and as we're forced to load the entire source
            into memory anyway because of this, if it's XML, we could easily put it into
            the DOM instead of parsing XML from a JavaScript variable. This interferes
            with XHTML GUI work in this XSLT file, but if the XML source would need to be
            escaped to be a JavaScript string literal, a conversion is unavoidable, so
            the XHTML file can just as well be the result of a XSLT conversion from the source.
        -->
        <div id="graphml-input" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program. It's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="/graphml:graphml"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//graphml:*">
    <xsl:element name="graphml:{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//graphml:*//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
