<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2021 Stephan Kreutzer

This file is part of dimgra-3.

dimgra-3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

dimgra-3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with dimgra-3. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:graphml="http://graphml.graphdrawing.org/xmlns">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by graphml_to_dimgra_xhtml_1.xsl of dimgra-3, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/hypertext-systems/dimgra-experimental/tree/master/dimgra/dimgra-3/ and https://hypertext-systems.org). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2021 Stephan Kreutzer

This file is part of dimgra-3.

dimgra-3 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

dimgra-3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with dimgra-3. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="graphml-input"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>dimgra-3</title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <style type="text/css">
body
{
    font-family: sans-serif;
    color: #FF0000;
    background-color: #000000;
}

a
{
    color: #FF8888;
}

hr
{
    border: none;
    height: 1px;
    background-color: #FF0000;
}

select
{
    color: #FF0000;
    background-color: #000000;
    border: 1px solid #FF0000;
}

#container-top-left
{
    width: 48%;
    height: 48%;
    position: absolute;
    top: 1%;
    left: 1%;
    overflow: auto;
    border: 1px solid #FF0000;
}

#pane-top-left
{
    text-align: center;
}

#container-top-right
{
    width: 48%;
    height: 48%;
    position: absolute;
    left: 51%;
    top: 1%;
    overflow: auto;
    border: 1px solid #FF0000;
}

#pane-top-right
{
    text-align: left;
}

#container-bottom-left
{
    width: 48%;
    height: 48%;
    position: absolute;
    left: 1%;
    top: 51%;
    overflow: auto;
    border: 1px solid #FF0000;
}

#pane-bottom-left
{
    text-align: left;
}

#container-bottom-right
{
    width: 48%;
    height: 48%;
    position: absolute;
    left: 51%;
    top: 51%;
    overflow: auto;
    border: 1px solid #FF0000;
}

#pane-bottom-right
{
    text-align: left;
}

#details
{
    width: 98%;
    height: 48%;
    position: absolute;
    left: 1%;
    top: 51%;
    border: 1px solid #FF0000;
}

#axis-x-dimension-1
{
    width: 100%;
}

#axis-x-dimension-2
{
    width: 100%;
}

#axis-x-dimension-3
{
    width: 100%;
}

#axis-y-dimension
{
    width: 100%;
}

.highlight
{
    background-color: #880000;
}
        </style>

        <xsl:comment> Engine </xsl:comment>
        <script type="text/javascript">
"use strict";

function GraphEngine()
{
    let that = this;

    // Pass the ID of the node, which is a string. Empty string to get the first,
    // default node. Returns null if the node can't be obtained.
    // TODO: When requesting the default, the actual ID of the first node should
    // be in the returned Node object.
    that.getNodeById = function(id) {
        if (typeof id !== 'string' &amp;&amp; !(id instanceof String))
        {
            id = "";
        }

        if (_loaded == false)
        {
            _load();
        }

        if (_loaded == false)
        {
            throw "Loading failed."
        }

        let nodeSource = null;

        if ("querySelectorAll" in document)
        {
            if (id.length &lt;= 0)
            {
                // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
                // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
                let result = document.querySelectorAll("node");

                for (let i = 0; i &lt; result.length; i++)
                {
                    if (result[i].namespaceURI == "http://graphml.graphdrawing.org/xmlns")
                    {
                        nodeSource = result[i];
                        break;
                    }
                }
            }
            else
            {
                // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
                // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
                let result = document.querySelectorAll("node[id='" + id + "']");
                let nodes = new Array();

                for (let i = 0; i &lt; result.length; i++)
                {
                    if (result[i].namespaceURI == "http://graphml.graphdrawing.org/xmlns")
                    {
                        nodes.push(result[i]);
                    }
                }

                if (nodes.length &lt;= 0)
                {

                }
                else if (nodes.length &gt; 1)
                {
                    throw "'id' attribute value '" + id + "' of a node found more than once.";
                }

                nodeSource = nodes[0];
            }
        }
        else
        {
            if (id.length &lt;= 0)
            {
                if (_nodeMapping.length &gt; 0)
                {
                    for (let item in _nodeMapping)
                    {
                        nodeSource = _nodeMapping[item];
                        break;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (!(id in _nodeMapping))
                {
                    return null;
                }

                nodeSource = _nodeMapping[id];
            }
        }

        if (nodeSource == null)
        {
            return null;
        }

        if (nodeSource.hasAttribute("id") != true)
        {
            throw "node is missing its 'id' attribute.";
        }

        let title = null;
        let description = null;

        for (let i = 0; i &lt; nodeSource.children.length; i++)
        {
            let child = nodeSource.children[i];

            if (child.namespaceURI != "http://graphml.graphdrawing.org/xmlns")
            {
                continue;
            }

            if (child.localName != "data")
            {
                continue;
            }

            if (child.hasAttribute("key") != true)
            {
                throw "data is missing its 'key' attribute.";
            }

            if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/node-title.20200601T122500Z")
            {
                if (title != null)
                {
                    throw "node contains more than one title.";
                }

                title = child.textContent;
            }
            else if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/node-text.20200601T122500Z")
            {
                if (description != null)
                {
                    throw "node contains more than one text.";
                }

                description = child.textContent;
            }
        }


        let parents = new Array();
        let children = new Array();

        if ("querySelectorAll" in document)
        {
            // TODO: Check nodeSource.getAttribute("id") for injection here and elsewhere.
            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            let result = document.querySelectorAll("edge[source='" + nodeSource.getAttribute("id") + "']");

            for (let i = 0; i &lt; result.length; i++)
            {
                if (result[i].namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                {
                    continue;
                }

                if (result[i].hasAttribute("source") != true)
                {
                    throw "edge is missing its 'source' attribute.";
                }

                if (result[i].hasAttribute("target") != true)
                {
                    throw "edge is missing its 'target' attribute.";
                }

                let dimension = null;
                let dimensionEstablishing = null;

                for (let j = 0; j &lt; result[i].children.length; j++)
                {
                    let child = result[i].children[j];

                    if (child.namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                    {
                        continue;
                    }

                    if (child.localName != "data")
                    {
                        continue;
                    }

                    if (child.hasAttribute("key") != true)
                    {
                        throw "data is missing its 'key' attribute.";
                    }

                    if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-target.20200601T122500Z")
                    {
                        if (dimension != null)
                        {
                            throw "edge contains more than one dimension.";
                        }

                        dimension = child.textContent;
                    }
                    else if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-establishing.20200601T122500Z")
                    {
                        if (dimensionEstablishing != null)
                        {
                            throw "edge contains more than one setting about if it is establishing a dimension.";
                        }

                        if (child.textContent == "true")
                        {
                            dimensionEstablishing = true;
                        }
                        else if (child.textContent == "false")
                        {
                            dimensionEstablishing = false;
                        }
                        else
                        {
                            throw "edge contains unknown setting value '" + child.textContent + "' regarding if it's establishing a dimension, 'true' or 'false' (bool) expected.";
                        }
                    }

                    if (dimension != null &amp;&amp;
                        dimensionEstablishing != null)
                    {
                        break;
                    }
                }

                if (dimension === null)
                {
                    dimension = "";
                    dimensionEstablishing = false;
                }

                if (dimensionEstablishing === null)
                {
                    dimensionEstablishing = false;
                }

                children.push(new Edge(result[i].getAttribute("target"), dimension, dimensionEstablishing));
            }


            // TODO: Check nodeSource.getAttribute("id") for injection here and elsewhere.
            // Evil browsers/web/W3C: no support for namespaces in querySelectorAll().
            // See https://www.w3.org/TR/selectors-api/#resolving-namespaces
            result = document.querySelectorAll("edge[target='" + nodeSource.getAttribute("id") + "']");

            for (let i = 0; i &lt; result.length; i++)
            {
                if (result[i].namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                {
                    continue;
                }

                if (result[i].hasAttribute("source") != true)
                {
                    throw "edge is missing its 'source' attribute.";
                }

                if (result[i].hasAttribute("target") != true)
                {
                    throw "edge is missing its 'target' attribute.";
                }

                let dimension = null;
                let dimensionEstablishing = null;

                for (let j = 0; j &lt; result[i].children.length; j++)
                {
                    let child = result[i].children[j];

                    if (child.namespaceURI != "http://graphml.graphdrawing.org/xmlns")
                    {
                        continue;
                    }

                    if (child.localName != "data")
                    {
                        continue;
                    }

                    if (child.hasAttribute("key") != true)
                    {
                        throw "data is missing its 'key' attribute.";
                    }

                    if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-target.20200601T122500Z")
                    {
                        if (dimension != null)
                        {
                            throw "edge contains more than one dimension.";
                        }

                        dimension = child.textContent;
                    }
                    else if (child.getAttribute("key") == "htx-scheme-id://org.hypertext-systems.20180702T071630Z/dimgra.20200601T122500Z/dimgra-1.20200601T122500Z/dimension-establishing.20200601T122500Z")
                    {
                        if (dimensionEstablishing != null)
                        {
                            throw "edge contains more than one setting about if it is establishing a dimension.";
                        }

                        if (child.textContent == "true")
                        {
                            dimensionEstablishing = true;
                        }
                        else if (child.textContent == "false")
                        {
                            dimensionEstablishing = false;
                        }
                        else
                        {
                            throw "edge contains unknown setting value '" + child.textContent + "' regarding if it's establishing a dimension, 'true' or 'false' (bool) expected.";
                        }
                    }

                    if (dimension != null &amp;&amp;
                        dimensionEstablishing != null)
                    {
                        break;
                    }
                }

                /*
                if (dimensionEstablishing !== true)
                {
                    dimension = null;
                }
                */

                if (dimension === null)
                {
                    dimension = "";
                    dimensionEstablishing = false;
                }

                if (dimensionEstablishing === null)
                {
                    dimensionEstablishing = false;
                }

                parents.push(new Edge(result[i].getAttribute("source"), dimension, dimensionEstablishing));
            }
        }
        else
        {
            // TODO: Get nodes from the native DOM XML loader.
        }

        return new Node(nodeSource.getAttribute("id"), title, description, parents, children);
    }

    function _load()
    {
        if (_loaded != false)
        {
            return true;
        }

        if ("querySelectorAll" in document)
        {
            _loaded = true;
            return true;
        }
        else
        {
            //TODO Implement native DOM XML loader.
        }
    }

    let _loaded = false;
}

function Node(id, title, description, parents, children)
{
    // TODO: Properly check if parents, children is an Array,
    // and throw exception if not.

    let that = this;

    that.getId = function()
    {
        return _id;
    }

    that.getTitle = function()
    {
        return _title;
    }

    that.getText = function()
    {
        return _description;
    }

    that.getParents = function()
    {
        return _parents;
    }

    that.getChildren = function()
    {
        return _children;
    }

    let _id = id;
    let _title = title;
    let _description = description;
    // Edge objects.
    let _parents = parents;
    // Edge objects.
    let _children = children;
}

function Edge(id, dimension, dimensionEstablishing)
{
    let that = this;

    that.getId = function()
    {
        return _id;
    }

    that.getDimension = function()
    {
        return _dimension;
    }

    that.isDimensionEstablishing = function()
    {
        return _dimensionEstablishing;
    }

    let _id = id;
    let _dimension = dimension;
    let _dimensionEstablishing = dimensionEstablishing;
}
        </script>

        <xsl:comment> Renderer </xsl:comment>
        <script type="text/javascript">
"use strict";

function UpdateDimensions(dimensions, form, target, currentSelection)
{
    let axisControl = document.getElementById(target);

    if (axisControl.children.length &lt;= 0)
    {
        let caption = document.createTextNode("");

        let option = document.createElement("option");
        option.setAttribute("value", "");
        option.appendChild(caption);

        if (currentSelection === null ||
            currentSelection === "")
        {
            option.setAttribute("selected", "selected");
        }

        axisControl.appendChild(option);
    }
    else
    {
        if (axisControl.children[0].getAttribute("value") !== "")
        {
            let caption = document.createTextNode("");

            let option = document.createElement("option");
            option.setAttribute("value", "");
            option.appendChild(caption);

            if (currentSelection === null ||
                currentSelection === "")
            {
                option.setAttribute("selected", "selected");
            }
            else
            {
                option.removeAttribute("selected");
            }

            axisControl.insertBefore(option, axisControl.firstChild);
        }
        else
        {
            if (currentSelection === null ||
                currentSelection === "")
            {
                axisControl.children[0].setAttribute("selected", "selected");
            }
            else
            {
                axisControl.children[0].removeAttribute("selected");
            }
        }
    }


    for (let i = axisControl.children.length - 1; i &gt; 0; i--)
    {
        let found = false;

        for (let j = 0; j &lt; dimensions.length; j++)
        {
            let dimension = dimensions[j];

            if (dimension === null)
            {
                continue;
            }

            if (axisControl.children[i].getAttribute("value") === dimension)
            {
                found = true;
                break;
            }
        }

        if (found == false)
        {
            axisControl.removeChild(axisControl.children[i]);
        }
    }


    for (let i = 0; i &lt; dimensions.length; i++)
    {
        let dimension = dimensions[i];

        if (dimension === null)
        {
            continue;
        }

        let found = false;

        for (let j = 1; j &lt; axisControl.children.length; j++)
        {
            if (axisControl.children[j].getAttribute("value") === dimension)
            {
                found = true;
            }

            if (axisControl.children[j].getAttribute("value") === currentSelection)
            {
                axisControl.children[j].setAttribute("selected", "selected");
            }
            else
            {
                axisControl.children[j].removeAttribute("selected");
            }
        }

        if (found == false)
        {
            let caption = document.createTextNode(dimension);

            let option = document.createElement("option");
            option.setAttribute("value", dimension);
            option.appendChild(caption);

            if (dimension === currentSelection)
            {
                option.setAttribute("selected", "selected");
            }

            axisControl.appendChild(option);
        }
    }

    if (form in document.forms)
    {
        document.forms[form].reset();
    }
    else
    {
        console.log("Can't find form with name \"" + form + "\".");
    }

    return true;
}

function RenderYAxis(before, node, after)
{
    return RenderNodes(new Array(before, node, after), "pane-top-left", "Current", null);
}

function RenderNodes(nodes, targetId, caption, dimension)
{
    // TODO: Properly check if nodes is an array.

    let target = document.getElementById(targetId);

    if (target == null)
    {
        throw "Renderer: Can't find target with ID '" + targetId + "'.";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode(caption));
    container.appendChild(header);


    if (targetId === "pane-top-left")
    {
        if (nodes.length &gt;= 3)
        {
            if (nodes.length &gt; 3)
            {

            }

            if (nodes[0] !== undefined &amp;&amp;
                nodes[0] !== null)
            {
                let title = document.createTextNode(nodes[0].getTitle());

                let span = document.createElement("span");
                span.setAttribute("onclick", "ShowDetails(\"" + nodes[0].getId() + "\");");
                span.appendChild(title);

                container.appendChild(span);

                container.appendChild(document.createElement("br"));

                let navigationLink = document.createElement("a");
                navigationLink.setAttribute("href", "javascript:void(0);");
                // TODO: Prevent injection (and maybe at other places, too).
                navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[0].getId() + "\", null, false);");

                let arrow = document.createTextNode("⬇");
                navigationLink.appendChild(arrow);
                container.appendChild(navigationLink);

                container.appendChild(document.createElement("hr"));
            }

            let title = document.createTextNode(nodes[1].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + nodes[1].getId() + "\");");
            span.setAttribute("class", "highlight");
            span.appendChild(title);

            container.appendChild(span);

            if (nodes[2] !== null &amp;&amp;
                nodes[2] !== null)
            {
                container.appendChild(document.createElement("hr"));

                let navigationLink = document.createElement("a");
                navigationLink.setAttribute("href", "javascript:void(0);");
                // TODO: Prevent injection (and maybe at other places, too).
                navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[2].getId() + "\", null, false);");

                let arrow = document.createTextNode("⬇");
                navigationLink.appendChild(arrow);
                container.appendChild(navigationLink);

                container.appendChild(document.createElement("br"));

                let title = document.createTextNode(nodes[2].getTitle());

                let span = document.createElement("span");
                span.setAttribute("onclick", "ShowDetails(\"" + nodes[2].getId() + "\");");
                span.appendChild(title);

                container.appendChild(span);
            }
        }
        else
        {

        }
    }
    else if (targetId === "pane-top-right" ||
             targetId === "pane-bottom-left" ||
             targetId === "pane-bottom-right")
    {
        for (let i = 0; i &lt; nodes.length; i++)
        {
            let navigationLink = document.createElement("a");
            navigationLink.setAttribute("href", "javascript:void(0);");
            // TODO: Prevent injection (and maybe at other places, too).
            navigationLink.setAttribute("onclick", "NavigateNode(\"" + nodes[i].getId() + "\", \"" + dimension + "\", false);");

            // http://xahlee.info/comp/unicode_BLACK_RIGHTWARDS_problem.html
            // let arrow = document.createTextNode("➡");
            let arrow = document.createTextNode("⮕");
            navigationLink.appendChild(arrow);
            container.appendChild(navigationLink);

            container.appendChild(document.createTextNode(" "));

            let title = document.createTextNode(nodes[i].getTitle());

            let span = document.createElement("span");
            span.setAttribute("onclick", "ShowDetails(\"" + nodes[i].getId() + "\");");
            span.appendChild(title);

            container.appendChild(span);

            /* if (i &lt; nodes.length - 1) */
            {
                container.appendChild(document.createElement("hr"));
            }
        }
    }
    else
    {
        throw "Renderer: Unknown target ID '" + targetId + "'.";
    }


    target.appendChild(container);

    return true;
}

function Reset()
{
    let targets = new Array("pane-top-left", "pane-top-right", "pane-bottom-left", "pane-bottom-right");

    for (let i = 0; i &lt; targets.length; i++)
    {
        let target = document.getElementById(targets[i]);

        if (target == null)
        {
            throw "Renderer: Can't find target with ID '" + targets[i] + "'.";
        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }
    }
}

function RenderDetails(node)
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"details\".";
    }

    let container = document.createElement("div");

    let header = document.createElement("h3");
    header.appendChild(document.createTextNode("Details"));
    container.appendChild(header);

    let description = node.getText();

    if (description != null)
    {
        container.appendChild(document.createTextNode(description));
    }

    target.appendChild(container);
    target.style.display = "block";

    target = document.getElementById("container-bottom-right");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-bottom-right\".";
    }

    target.style.display = "none";

    target = document.getElementById("container-bottom-left");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-bottom-left\".";
    }

    target.style.display = "none";

    return 0;
}

function ResetDetails()
{
    let target = document.getElementById("details");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"details\".";
    }

    while (target.hasChildNodes() == true)
    {
        target.removeChild(target.lastChild);
    }

    target.style.display = "none";

    target = document.getElementById("container-bottom-right");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-bottom-right\".";
    }

    target.style.display = "block";

    target = document.getElementById("container-bottom-left");

    if (target == null)
    {
        throw "Renderer: Can't find target with ID \"container-bottom-left\".";
    }

    target.style.display = "block";
}
        </script>

        <xsl:comment> Navigation </xsl:comment>
        <script type="text/javascript">
"use strict";

let currentNode = null;
let currentXAxisDimension1 = null;
let currentXAxisDimension2 = null;
let currentXAxisDimension3 = null;
let currentYAxisDimension = null;
let graphEngine = new GraphEngine();

// TODO: This should be optimized by caching xParents and xChildren
// that were already loaded/interpreted for the current node.

function NavigateNode(nodeId, dimensionalPivotFrom, dimensionsReset)
{
    currentNode = null;

    if (dimensionsReset === true)
    {
        currentXAxisDimension1 = null;
        currentXAxisDimension2 = null;
        currentXAxisDimension3 = null;
        currentYAxisDimension = null;
    }

    ResetDetails();
    Reset();

    let node = graphEngine.getNodeById(nodeId);

    if (node == null)
    {
        throw "Loading a node failed.";
    }

    if (dimensionalPivotFrom == null)
    {
        let currentXAxisDimensions = new Array(currentXAxisDimension1, currentXAxisDimension2, currentXAxisDimension3);

        for (let i = 0, max = currentXAxisDimensions.length; i &lt; max; i++)
        {
            if (currentXAxisDimensions[i] === null)
            {
                continue;
            }

            let found = false;

            for (let j = 0, max2 = node.getChildren().length; j &lt; max2; j++)
            {
                let dimension = node.getChildren()[j].getDimension();

                if (dimension === null)
                {
                    continue;
                }

                if (currentYAxisDimension !== null)
                {
                    if (dimension === currentYAxisDimension)
                    {
                        continue;
                    }
                }

                if (dimension === currentXAxisDimensions[i])
                {
                    found = true;
                    break;
                }
            }

            if (found === false)
            {
                currentXAxisDimensions[i] = null;
            }
        }

        currentXAxisDimension1 = currentXAxisDimensions[0];
        currentXAxisDimension2 = currentXAxisDimensions[1];
        currentXAxisDimension3 = currentXAxisDimensions[2];
    }
    else
    {
        let previousYAxisDimension = currentYAxisDimension;
        let currentXAxisDimensions = new Array(currentXAxisDimension1, currentXAxisDimension2, currentXAxisDimension3);

        for (let i = 0, max = currentXAxisDimensions.length; i &lt; max; i++)
        {
            if (currentXAxisDimensions[i] == dimensionalPivotFrom)
            {
                currentXAxisDimensions[i] = previousYAxisDimension;
                break;
            }
        }

        currentYAxisDimension = dimensionalPivotFrom;
        currentXAxisDimension1 = currentXAxisDimensions[0];
        currentXAxisDimension2 = currentXAxisDimensions[1];
        currentXAxisDimension3 = currentXAxisDimensions[2];
    }


    if (currentYAxisDimension === null)
    {
        for (let i = 0; i &lt; node.getParents().length; i++)
        {
            if (node.getParents()[i].isDimensionEstablishing() === true)
            {
                currentYAxisDimension = node.getParents()[i].getDimension();

                if (currentYAxisDimension === null)
                {
                    currentYAxisDimension = "";
                }

                break;
            }
        }

        if (currentYAxisDimension === null)
        {
            for (let i = 0; i &lt; node.getChildren().length; i++)
            {
                if (node.getChildren()[i].isDimensionEstablishing() === true)
                {
                    currentYAxisDimension = node.getChildren()[i].getDimension();

                    if (currentYAxisDimension === null)
                    {
                        currentYAxisDimension = "";
                    }

                    break;
                }
            }
        }

        if (currentYAxisDimension === null)
        {
            // No prior knowledge about dimensions pointing inwards.
            currentYAxisDimension = "";
        }
    }

    {
        let currentXAxisDimensions = new Array(currentXAxisDimension1, currentXAxisDimension2, currentXAxisDimension3);
        let i = 0;
        let max = currentXAxisDimensions.length;
        let j = 0;
        let max2 = node.getChildren().length;

        for (; i &lt; max &amp;&amp; j &lt; max2; i++)
        {
            if (currentXAxisDimensions[i] !== null)
            {
                continue;
            }

            for (; j &lt; max2; j++)
            {
                if (node.getChildren()[j].getDimension() === null)
                {
                    continue;
                }

                if (currentYAxisDimension !== null)
                {
                    if (node.getChildren()[j].getDimension() === currentYAxisDimension)
                    {
                        continue;
                    }
                }

                let skip = false;

                for (let k = 0; k &lt; max; k++)
                {
                    if (currentXAxisDimensions[k] !== null &amp;&amp;
                        currentXAxisDimensions[k] === node.getChildren()[j].getDimension())
                    {
                        skip = true;
                        break;
                    }
                }

                if (skip == true)
                {
                    continue;
                }

                currentXAxisDimensions[i] = node.getChildren()[j].getDimension();
                break;
            }
        }

        currentXAxisDimension1 = currentXAxisDimensions[0];
        currentXAxisDimension2 = currentXAxisDimensions[1];
        currentXAxisDimension3 = currentXAxisDimensions[2];
    }


    let xChildren1 = new Array();
    let xChildren2 = new Array();
    let xChildren3 = new Array();
    let yChildren = new Array();
    let xDimensions = new Array();

    for (let i = 0; i &lt; node.getChildren().length; i++)
    {
        let dimension = node.getChildren()[i].getDimension();

        if (dimension === null)
        {
            dimension = "";
        }

        if (currentYAxisDimension !== null)
        {
            if (dimension === currentYAxisDimension)
            {
                if (yChildren.length &gt; 1)
                {
                    console.log("y-axis with more than one child.");
                    continue;
                }

                yChildren.push(graphEngine.getNodeById(node.getChildren()[i].getId()));
                continue;
            }
        }

        if (currentXAxisDimension1 !== null)
        {
            if (dimension === currentXAxisDimension1)
            {
                xChildren1.push(graphEngine.getNodeById(node.getChildren()[i].getId()));
            }
        }

        if (currentXAxisDimension2 !== null)
        {
            if (dimension === currentXAxisDimension2)
            {
                xChildren2.push(graphEngine.getNodeById(node.getChildren()[i].getId()));
            }
        }

        if (currentXAxisDimension3 !== null)
        {
            if (dimension === currentXAxisDimension3)
            {
                xChildren3.push(graphEngine.getNodeById(node.getChildren()[i].getId()));
            }
        }

        if (xDimensions.includes(node.getChildren()[i].getDimension()) != true)
        {
            xDimensions.push(node.getChildren()[i].getDimension());
        }
    }


    {
        let before = null;
        let after = null;

        if (yChildren.length &gt; 0)
        {
            after = yChildren[0];
        }

        for (let i = 0; i &lt; node.getParents().length; i++)
        {
            if (node.getParents()[i].isDimensionEstablishing() === true)
            {
                if (node.getParents()[i].getDimension() === currentYAxisDimension)
                {
                    before = graphEngine.getNodeById(node.getParents()[i].getId());
                    break;
                }
            }
        }

        if (RenderYAxis(before, node, after, currentYAxisDimension) == true)
        {
            currentNode = node.getId();
        }
        else
        {
            return false;
        }
    }


    if (UpdateDimensions(xDimensions, "form-axis-x-dimension-1", "axis-x-dimension-1", currentXAxisDimension1) != true)
    {
        return false;
    }

    if (RenderNodes(xChildren1, "pane-top-right", "Pane-Top-Right", currentXAxisDimension1) != true)
    {
        return false;
    }

    if (UpdateDimensions(xDimensions, "form-axis-x-dimension-2", "axis-x-dimension-2", currentXAxisDimension2) != true)
    {
        return false;
    }

    if (RenderNodes(xChildren2, "pane-bottom-left", "Pane-Bottom-Left", currentXAxisDimension2) != true)
    {
        return false;
    }

    if (UpdateDimensions(xDimensions, "form-axis-x-dimension-3", "axis-x-dimension-3", currentXAxisDimension3) != true)
    {
        return false;
    }

    if (RenderNodes(xChildren3, "pane-bottom-right", "Pane-Bottom-Right", currentXAxisDimension3) != true)
    {
        return false;
    }

    if (UpdateDimensions(new Array(currentYAxisDimension), "form-axis-y-dimension", "axis-y-dimension", currentYAxisDimension) != true)
    {
        return false;
    }

    return true;
}

function ShowDetails(nodeId)
{
    ResetDetails();

    let node = graphEngine.getNodeById(nodeId);

    if (node == null)
    {
        throw "Loading a node failed.";
    }

    if (RenderDetails(node) != 0)
    {
        return -1;
    }

    return 0;
}

function ChangeAxisDimension(axis, container)
{
    let axisControl = null;

    if (axis == "x")
    {
        axisControl = document.getElementById("axis-x-dimension-" + container);

        if (axisControl == null)
        {

        }
    }
    else if (axis == "y")
    {
        axisControl = document.getElementById("axis-y-dimension");

        if (axisControl == null)
        {

        }
    }
    else
    {
        throw "Unknown axis \"" + axis + "\".";
    }

    if (currentYAxisDimension === axisControl.options[axisControl.selectedIndex].value)
    {
        currentYAxisDimension = null;
    }

    if (currentXAxisDimension1 === axisControl.options[axisControl.selectedIndex].value)
    {
        currentXAxisDimension1 = null;
    }

    if (currentXAxisDimension2 === axisControl.options[axisControl.selectedIndex].value)
    {
        currentXAxisDimension2 = null;
    }

    if (currentXAxisDimension3 === axisControl.options[axisControl.selectedIndex].value)
    {
        currentXAxisDimension3 = null;
    }


    if (axis == "x")
    {
        if (container === 1)
        {
            currentXAxisDimension1 = axisControl.options[axisControl.selectedIndex].value;
        }
        else if (container === 2)
        {
            currentXAxisDimension2 = axisControl.options[axisControl.selectedIndex].value;
        }
        else if (container === 3)
        {
            currentXAxisDimension3 = axisControl.options[axisControl.selectedIndex].value;
        }
        else
        {
            throw "Unknown container " + container + ".";
        }
    }
    else if (axis == "y")
    {
        let axisControl = document.getElementById("axis-y-dimension");

        if (axisControl == null)
        {

        }

        currentYAxisDimension = axisControl.options[axisControl.selectedIndex].value;
    }
    else
    {
        throw "Unknown axis \"" + axis + "\".";
    }

    NavigateNode(currentNode, null, false);
}
        </script>

        <script type="text/javascript">
<xsl:text>
"use strict";

// As browsers/W3C/WHATWG are evil, they might ignore the self-declarative XML
// namespace of this document and the given content type in the header, and instead
// assume/render "text/html", which then fails with JavaScript characters that are
// XML/XHTML special characters which need to be escaped, if the file is saved under
// a name that happens to end with ".html" (!!!). Just have the file name end with
// ".xhtml" and it might magically start to work.

function loadGraph()
{
    if (window.location.hash.length &gt; 1)
    {
        NavigateNode(window.location.hash.substr(1), null, true);
    }
    else
    {
        // NavigateNode() with empty ID string: load the "default" (first) node
        // of the first dimension.
        NavigateNode("", null, true);
    }
}

window.onload = function () {
    let loadLink = document.getElementById('loadLink');
    loadLink.parentNode.removeChild(loadLink);

    loadGraph();

    if ("onhashchange" in window)
    {
        window.onhashchange = loadGraph;
    }
};
</xsl:text>
        </script>
      </head>
      <body>
        <div id="graph">
          <div id="container-top-left">
            <form name="form-axis-y-dimension">
              <select id="axis-y-dimension" size="1" onchange="ChangeAxisDimension('y', 0);" autocomplete="off">
              </select>
            </form>
            <div id="pane-top-left"/>
          </div>
          <div id="container-top-right">
            <form name="form-axis-x-dimension-1">
              <select id="axis-x-dimension-1" size="1" onchange="ChangeAxisDimension('x', 1);" autocomplete="off">
              </select>
            </form>
            <div id="pane-top-right"/>
          </div>
          <div id="container-bottom-left">
            <form name="form-axis-x-dimension-2">
              <select id="axis-x-dimension-2" size="1" onchange="ChangeAxisDimension('x', 2);" autocomplete="off">
              </select>
            </form>
            <div id="pane-bottom-left"/>
          </div>
          <div id="container-bottom-right">
            <form name="form-axis-x-dimension-3">
              <select id="axis-x-dimension-3" size="1" onchange="ChangeAxisDimension('x', 3);" autocomplete="off">
              </select>
            </form>
            <div id="pane-bottom-right"/>
          </div>
          <div id="details"/>
          <div id="loadLink">
            <a href="#" onclick="loadGraph();">Load</a>
          </div>
        </div>

        <!--
            As the stupid browser execution environment is sandboxed, local files can't
            be dynamically read from disk, and as we're forced to load the entire source
            into memory anyway because of this, if it's XML, we could easily put it into
            the DOM instead of parsing XML from a JavaScript variable. This interferes
            with XHTML GUI work in this XSLT file, but if the XML source would need to be
            escaped to be a JavaScript string literal, a conversion is unavoidable, so
            the XHTML file can just as well be the result of a XSLT conversion from the source.
        -->
        <div id="graphml-input" style="display:none;">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program. It's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="/graphml:graphml"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//graphml:*">
    <xsl:element name="graphml:{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()|text()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//graphml:*//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="text()"/>

</xsl:stylesheet>
